<?php


class Router
{
    private $routes;

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $routesPath = ROOT.'/config/routes.php';
        $this->routes = include($routesPath);
    }

    /**
     * @return string
     */
    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI']);
        }
    }

    public function run()
    {
        # get query string
        $uri = $this->getURI();

        # check that in routes.php
        foreach ($this->routes as $pattern => $route) {
            if (preg_match("~/$pattern/~", $uri)) {
                $segments = explode('/', $route);

                # check controller & actions
                $controllerName = array_shift($segments).'Controller';
                $controllerName = ucfirst($controllerName);

                $actionName = 'action'.ucfirst(array_shift($segments));

                $controllerFile = ROOT.'/controllers/'.$controllerName.'.php';
                if (file_exists($controllerFile)){
                    include_once($controllerFile);
                }

                # connect controller
                $controller = new $controllerName;
                $result = $controller->$actionName();
                if ($result) {
                    break;
                }
            }
        }

        # create object & call the method
    }
}